#include<dht11.h>
dht11 DHT11;
#define DHT11PIN 2  
#include <Process.h>
#include <stdlib.h>

#define ON      HIGH
#define OFF     LOW
int aa;
int bb;
int cc;
int ll;
int hhm;
int iter =0;
void setup() {
  Serial.begin(9600);
  Bridge.begin();
  pinMode(7, OUTPUT);
  pinMode(4,OUTPUT);
  digitalWrite(7,LOW);
  digitalWrite(4,LOW);
  
}

void photorez_sensor()
{
  Process opo;        
   opo.begin("curl");
   char str[12];
   String lightoptions ="http://192.168.240.107/Settings/GetLightMode";
    opo.addParameter(lightoptions);
  opo.run();
  while (opo.available()>0) {
    char opu = opo.read();
   
    
  Serial.println("*************************************");
  
  Serial.print("Wartosc pomiaru swiatla: ");
 ll = analogRead(A1);
Serial.println(ll);
  delay(5000);
  if (opu == '0')
  {
    Serial.println("**Swiatla uruchamiaja sie automatyczne!**");
  if(ll>200)
  {
    digitalWrite(7, ON);
    Serial.println("--Włączono światło automatycznie!--");
  }
  
  if(ll<200)
  {
    ;
    digitalWrite(7, OFF);
    Serial.println("--Wyłączono światło automatycznie!--");
  }

  }
  if (opu == '1')
  {
   Serial.println("**Swiatla uruchamiaja sie manualnie**");
    Process lig;        
   lig.begin("curl");
   char str[12];
   String light ="http://192.168.240.107/Settings/GetLightManualMode";
    lig.addParameter(light);
  lig.run();
  while (lig.available()>0) {
    char ligh = lig.read();
    
  
  if(ligh == '1')
  {
  digitalWrite(7, ON);
  Serial.println("--Włączono światło manualnie!--");
  }
  if(ligh == '0')
  {
    digitalWrite(7, OFF);
    Serial.println("--Wyłączono światło manualnie!--");
  }
  }
  Serial.flush();
  }
  
}
Serial.flush();
delay(1000);
}
void moisure_sensor_one()
{ 
  Serial.println("*************************************");
  aa =analogRead(A0);
  if(aa<300)
  {
  Serial.print("Czujnik nr 1.Wilgotnosc gleby:");
  
  Serial.println(aa);
  Serial.println("\t\t") ;
  Serial.println("Sucha gleba");
   
  }
  else if(aa>300 and aa<700)
  {
  Serial.print("Czujnik nr 1.Wilgotnosc gleby:");  
  Serial.println(aa);  
  Serial.println("\t\t");
  Serial.println("Wilgotna gleba");
  }
    else if(aa>700)
  { 
  Serial.print("Czujnik nr 1.Wilgotnosc gleby:");
  Serial.println(aa);  
  Serial.println("\t\t");
  Serial.println("Za bardzo nawodniona gleba");  
  }
  delay(1000);
}

void moisure_sensor_two()
{ 
  Serial.println("*************************************");
  bb = analogRead(A2);
  if(bb>500)
  {
  Serial.print("Czujnik nr 2.Wilgotnosc gleby:");
  Serial.println(bb);
  Serial.println("\t\t") ;
  Serial.println("Sucha gleba");
   
  }
  else if(bb<500)
  {
  Serial.print("Czujnik nr 2.Wilgotnosc gleby:");  
  Serial.println(bb);  
  Serial.println("\t\t");
  Serial.println("Wilgotna gleba");
  }
  delay(1000);
}

void dht_sensor()
{
Serial.println("*************************************");
  //automat/manual
  Process fan;        
   fan.begin("curl");
   char str[12];
   String fanoption ="http://192.168.240.107/Settings/GetVentMode";
    fan.addParameter(fanoption);
  fan.run();
  while (fan.available()>0) {
    char fann = fan.read(); 
  //Serial.print("auto/manual-");
  //Serial.println(fann);
  
  int chk = DHT11.read(DHT11PIN);         
  Serial.print("Wilgotnosc powietrza(%): ");              
  hhm=DHT11.humidity;
  Serial.print(hhm);
  Serial.print("\t\t");
  Serial.print("Temperatura powietrza (C): ");   
  cc=DHT11.temperature;        
  Serial.println(cc);
  Serial.println("");

  if(fann == '1')
  {
    Serial.println("**Wiatrak uruchamia sie automatycznie**");
    
  if(cc > 30)
  {
    digitalWrite(4, ON); 
    Serial.println("--Włączono wiatrak automatycznie!--");  
  }
  
  if(cc < 30)
  {
    digitalWrite(4, OFF);
    Serial.println("--Wyłączono wiatrak automatycznie!--");
  }
  
  }
  if(fann == '0')
  {
    
    Serial.println("Wiatrak uruchamia sie manualnie");
    Process fano;        
   fano.begin("curl");
   char str[12];
   String fanonoff ="http://192.168.240.107/Settings/GetVentManualMode";
    fano.addParameter(fanonoff);
  fano.run();
  while (fano.available()>0) {
    char fanoo = fano.read(); 
  
    
    if(fanoo == '1')
  {
    digitalWrite(4, ON);   
    Serial.println("--Włączono wiatrak manualnie!--");
  }
  if(fanoo == '0')
  {
    digitalWrite(4, OFF);
    Serial.println("--Wyłączono wiatrak manualnie!--");
  }
  
  }
  Serial.flush();
  
  
}
  }
  Serial.flush();
  delay(1000);
}
void senddata()
{
  {
 Serial.println("*************************************");
   Process rr;        
   rr.begin("curl");
   char str[12];
    
  String airtemp ="http://192.168.240.107/DataSensors/atemperature?airTemperatureData=";
  
  airtemp=airtemp +cc;
  Serial.print(airtemp);
  Serial.println("\t\t") ;
  rr.addParameter(airtemp);
  rr.run();
  while (rr.available()>0) {
    char qq = rr.read();
    Serial.print(qq);
   }
   Serial.flush();
  }
  {
Process pp;        
   pp.begin("curl");
  char str3[12];
    
  String soilhumidity = "http://192.168.240.107/DataSensors/shumidity?soilHumidityData1=";
  soilhumidity = soilhumidity + aa;
  soilhumidity= soilhumidity + "&soilHumidityData2=" + bb;
  Serial.print(soilhumidity);
  Serial.println("\t\t") ;
  pp.addParameter(soilhumidity );
  pp.run();
  while (pp.available()>0) {
    char dd = pp.read();
    Serial.print(dd);
   }
   Serial.flush();
  }
  {
Process uu;        
   uu.begin("curl");
   char str2[12];
    
  String airhumidity ="http://192.168.240.107/datasensors/ahumidity?airHumidityData=";
  airhumidity=airhumidity + hhm;
  Serial.print(airhumidity);
  Serial.println("\t\t") ;
  uu.addParameter(airhumidity);
  uu.run();
  while (uu.available()>0) {
    char oo = uu.read();
    Serial.print(oo);
   }
   Serial.flush();
  }
  
{
Process ww;        
   ww.begin("curl");
  char str4[12];
  String lightsen = "http://192.168.240.107/DataSensors/LSensor?lighteData=";
  lightsen = lightsen + ll;
  
  Serial.print(lightsen);
  Serial.println("\t\t") ;
  ww.addParameter(lightsen );
  ww.run();
  while (ww.available()>0) {
    char ee = ww.read();
    Serial.print(ee);
   } 
   Serial.flush();
 
}
}
void loop()
{
  iter++;
 
  photorez_sensor();
  moisure_sensor_one();
  moisure_sensor_two();
  dht_sensor();
  if (iter == 10)
  {
  senddata();
  iter =0;
  }
}

